
//2. Use the project operator to display fruits per country

db.fruits.aggregate([
			{ $unwind : "$origin" },
			{ $group: {_id : "$origin" , fruitsPerCountry: {sum:1}}}
		]);


//3. Use the count operator to count the total number of fruits on sale.

db.fruits.aggregate([

{$match:{onSale: true}},
{$count: "fruitsOnSale"}

])

//3. Use the count operator to count the total number of fruits with stock more than 20.

db.fruits.aggregate([
{$match: {stock:{$gte:20}}},
{$count: "enoughStock"}

	])

//4. Use the average operator to get the average price of fruits onSale per supplier.

db.fruits.aggregate([
{$match: {onSale:true}},
{$group: {_id:"$supplier_id", avg_price: {$avg:"$price"}}}

	])

//5. Use the max operator to get the highest price of a fruit per supplier.

db.fruits.aggregate([
{$match: {onSale:true}},
{$group: {_id:"$supplier_id", max_price: {$max:"$price"}}}

	])

//6. Use the min operator to get the lowest price of a fruit per supplier.

db.fruits.aggregate([
{$match: {onSale:true}},
{$group: {_id:"$supplier_id", min_price: {$min:"$price"}}}

	])

/*
Activity:



7. Create a git repository named S25.
8. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code. "s25 Activity"
9. Add the link in Boodle."Aggregation in MongoDB and Query Case Studies"

*/